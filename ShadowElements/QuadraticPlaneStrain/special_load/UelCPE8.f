C==============================User element example Q8 plane stress
C==============================modified from
C==http://madamcode.blogspot.com/2017/02/simple-uel-for-abaqus-plane-stress-2d.html
      SUBROUTINE UEL(RHS,AMATRX,SVARS,ENERGY,NDOFEL,NRHS,NSVARS,
     1 PROPS,NPROPS,COORDS,MCRD,NNODE,U,DU,V,A,JTYPE,TIME,DTIME,
     2 KSTEP,KINC,JELEM,PARAMS,NDLOAD,JDLTYP,ADLMAG,PREDEF,
     3 NPREDF,LFLAGS,MLVARX,DDLMAG,MDLOAD,PNEWDT,JPROPS,NJPROP,
     4 PERIOD)
  
C==============================Allocation of variables/arrays and initialization
      IMPLICIT REAL*8 (A-H,O-Z) 

      DIMENSION RHS(MLVARX,1),AMATRX(NDOFEL,NDOFEL),
     1 SVARS(NSVARS),ENERGY(8),PROPS(*),COORDS(MCRD,NNODE),
     2 U(NDOFEL),DU(MLVARX,*),V(NDOFEL),A(NDOFEL),TIME(2),
     3 PARAMS(3),JDLTYP(MDLOAD,*),ADLMAG(MDLOAD,*),
     4 DDLMAG(MDLOAD,*),PREDEF(2,NPREDF,NNODE),LFLAGS(*),
     5 JPROPS(*)
     
	 
      REAL*8 rg(9),sg(9),wrg(9),wsg(9),
     1  DNDR_DS(2,4),JAC(2,2),JINV(2,2),DNDX_DY(2,8),
     2  B(3,NDOFEL),P(NDOFEL,NDOFEL),D(3,3),
     3  STRESS(3),STRAIN(3),JDET,E,NU,CONST,GP,atemp,btemp,DNDX_DY_NEW(2,8),DNDR_DS_NEW(2,8),B_NEW(3,16)

	  INTEGER ig,i
      include 'koutvar.inc.f' 
	  
	  data elemshift /0/  ! Set variable equal to 0 as initial value
	  	  	  
C==============================importing material properties from inputfile
      E    = PROPS(1)
      NU   = PROPS(2)
C==============================initializing AMATRX and RHS to 0
       RHS(:,1) = 0.d0
       AMATRX = 0.d0
C==============================Defining D the constitutive matrix (for plane stress analysis)       
       CONST = E/((1.0d0+NU)*(1.0d0-2.0d0*NU))       
      D(1,1)=1.0d0-NU; D(1,2)=NU;     D(1,3)=0.0d0
      D(2,1)=NU;     D(2,2)=1.0d0-NU; D(2,3)=0.0d0
      D(3,1)=0.0d0;   D(3,2)=0.0d0;   D(3,3)=0.5d0*(1.0d0-2.0d0*NU)
      D=CONST*D
C==============================Defining Gauss points and weighting factors (following abaqus convention for order of points)
      GP=0.7745966692			!Gauss point constant
	  atemp=0.5555555556
	  btemp=0.8888888889
      rg(:)=(/-GP,0.0d0,GP,-GP,0.0d0,GP,-GP,0.0d0,GP/)				!ZETA coordinates
      wrg(:)=(/atemp,btemp,atemp,atemp,btemp,atemp,atemp,btemp,atemp/)	!weighting factors zeta
      sg(:)=(/-GP,-GP,-GP,0.0d0,0.0d0,0.0d0,GP,GP,GP/)				!eta coordinates
      wsg(:)=(/atemp,atemp,atemp,btemp,btemp,btemp,atemp,atemp,atemp/)	!weighting factors eta
C==============================Looping over Gauss points
	ig=1
      DO ig=1,9     
C==============================Derivative of shape functions in respect to dr=zeta, ds=eta coordinates  
C==============================and evaluated at the current Gauss point
         dndr_ds(1,1) = -(1.D0-sg(ig))/4.0d0		!N1_dr
         dndr_ds(1,2) =  (1.D0-sg(ig))/4.0d0		!N2_dr
         dndr_ds(1,3) =  (1.D0+sg(ig))/4.0d0		!N3_dr
         dndr_ds(1,4) = -(1.D0+sg(ig))/4.0d0		!N4_dr
											
         dndr_ds(2,1) = -(1.D0-rg(ig))/4.0d0		!N1_ds
         dndr_ds(2,2) = -(1.D0+rg(ig))/4.0d0		!N2_ds
         dndr_ds(2,3) =  (1.D0+rg(ig))/4.0d0		!N3_ds
         dndr_ds(2,4) =  (1.D0-rg(ig))/4.0d0		!N4_ds
		 
		 dndr_ds_new(1,1)=-(sg(ig)-1)*(sg(ig)+2*rg(ig))/4.0d0
		 dndr_ds_new(1,2)=(sg(ig)-2*rg(ig))*(sg(ig)-1)/4.0d0
		 dndr_ds_new(1,3)=(sg(ig)+2*rg(ig))*(1+sg(ig))/4.0d0
		 dndr_ds_new(1,4)=-(sg(ig)-2*rg(ig))*(1+sg(ig))/4.0d0
		 dndr_ds_new(1,5)=(sg(ig)-1)*rg(ig)
		 dndr_ds_new(1,6)=-sg(ig)*sg(ig)*0.5d0+0.5d0
		 dndr_ds_new(1,7)=-(1+sg(ig))*rg(ig)
		 dndr_ds_new(1,8)=sg(ig)*sg(ig)*0.5d0-0.5d0
		 
		 dndr_ds_new(2,1)=-(rg(ig)-1)*(rg(ig)+2*sg(ig))/4.0d0
		 dndr_ds_new(2,2)=-(rg(ig)+1)*(rg(ig)-2*sg(ig))/4.0d0
		 dndr_ds_new(2,3)=(rg(ig)+1)*(rg(ig)+2*sg(ig))/4.0d0
		 dndr_ds_new(2,4)=(rg(ig)-1)*(rg(ig)-2*sg(ig))/4.0d0
		 dndr_ds_new(2,5)=rg(ig)*rg(ig)*0.5d0-0.5d0
		 dndr_ds_new(2,6)=-(rg(ig)+1)*sg(ig)
		 dndr_ds_new(2,7)=-rg(ig)*rg(ig)*0.5d0+0.5d0
		 dndr_ds_new(2,8)=(rg(ig)-1)*sg(ig)
         
C==============================Calculating the Jacobian Matrix = JAC
         JAC = 0.d0
          DO i=1,8
            JAC(1,1) = JAC(1,1) + dndr_ds_new(1,i)*COORDS(1,i)
            JAC(1,2) = JAC(1,2) + dndr_ds_new(1,i)*COORDS(2,i)
            JAC(2,1) = JAC(2,1) + dndr_ds_new(2,i)*COORDS(1,i)
            JAC(2,2) = JAC(2,2) + dndr_ds_new(2,i)*COORDS(2,i)
          END DO
C==============================Determinant of the Jacobian Matrix = JDET       
         JDET = JAC(1,1)*JAC(2,2)-JAC(2,1)*JAC(1,2)   
C==============================Inverse of Jacobian = JINV                 
         JINV(1,1) =  JAC(2,2)/JDET
         JINV(1,2) = -JAC(1,2)/JDET
         JINV(2,1) = -JAC(2,1)/JDET
         JINV(2,2) =  JAC(1,1)/JDET	 
C==============================Derivative of shape functions in respect to x, y coordinates
		 DNDX_DY=matmul(JINV,DNDR_DS_NEW)
C==============================Defining strain displacement matrix = B		 
		 B=0.d0
		 B(1,1)=DNDX_DY(1,1);	B(1,3)=DNDX_DY(1,2);	B(1,5)=DNDX_DY(1,3);	B(1,7)=DNDX_DY(1,4);
		 B(2,2)=DNDX_DY(2,1);	B(2,4)=DNDX_DY(2,2);	B(2,6)=DNDX_DY(2,3);	B(2,8)=DNDX_DY(2,4);
		 B(3,1)=DNDX_DY(2,1);	B(3,2)=DNDX_DY(1,1);	B(3,3)=DNDX_DY(2,2);	B(3,4)=DNDX_DY(1,2);
		 B(3,5)=DNDX_DY(2,3);	B(3,6)=DNDX_DY(1,3);	B(3,7)=DNDX_DY(2,4);	B(3,8)=DNDX_DY(1,4);
		 
		 B(1,1)=DNDX_DY(1,1);	B(1,3)=DNDX_DY(1,2);	B(1,5)=DNDX_DY(1,3);	B(1,7)=DNDX_DY(1,4); 	B(1,9)=DNDX_DY(1,5);	B(1,11)=DNDX_DY(1,6);	B(1,13)=DNDX_DY(1,7);	B(1,15)=DNDX_DY(1,8);
		 B(2,2)=DNDX_DY(2,1);	B(2,4)=DNDX_DY(2,2);	B(2,6)=DNDX_DY(2,3);	B(2,8)=DNDX_DY(2,4);	B(2,10)=DNDX_DY(2,5);	B(2,12)=DNDX_DY(2,6);	B(2,14)=DNDX_DY(2,7);	B(2,16)=DNDX_DY(2,8);
		 B(3,1)=DNDX_DY(2,1);	B(3,2)=DNDX_DY(1,1);	B(3,3)=DNDX_DY(2,2);	B(3,4)=DNDX_DY(1,2);
		 B(3,5)=DNDX_DY(2,3);	B(3,6)=DNDX_DY(1,3);	B(3,7)=DNDX_DY(2,4);	B(3,8)=DNDX_DY(1,4);
		 
		 B(3,9)=DNDX_DY(2,5);	B(3,10)=DNDX_DY(1,5);	B(3,11)=DNDX_DY(2,6);	B(3,12)=DNDX_DY(1,6);
		 B(3,13)=DNDX_DY(2,7);	B(3,14)=DNDX_DY(1,7);	B(3,15)=DNDX_DY(2,8);	B(3,16)=DNDX_DY(1,8);
				 
C==============================Calculating the integrand = P        
         P=matmul(matmul(transpose(B),D),B)*JDET
C==============================Calculating the stiffness matrix = AMATRX        
        AMATRX = AMATRX+P*wrg(ig)*wsg(ig)
C==============================Calculating the right hand side = RHS
        STRAIN = MATMUL(B,U)
        STRESS = MATMUL(D,STRAIN)
C==============================Save output to common block
 	     emodout(ig,jelem)=E
	    strain11out(ig,jelem)=STRAIN(1)
	    strain22out(ig,jelem)=STRAIN(2)
		strain12out(ig,jelem)=STRAIN(3)
		stress11out(ig,jelem)=STRESS(1)
		stress22out(ig,jelem)=STRESS(2)
		stress12out(ig,jelem)=STRESS(3)
		
		 elemshift=max(elemshift,jelem)
		 elemout(jelem)=jelem
		
       END DO
		RHS(:,1) = -MATMUL((AMATRX),U)
      END
	  

C----------------------------------------------------------------end uel.for
C===Modified from Lars Mikkelsen
C----------------------OUTPUT--------------------------------------Start
      SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     1 RPL,DDSDDT,DRPLDE,DRPLDT,
     2 STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME,
     3 NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     4 CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC) 

      INCLUDE 'ABA_PARAM.INC' 
      include 'koutvar.inc.f' 

      CHARACTER*80 CMNAME
      DIMENSION STRESS(NTENS),STATEV(NSTATV),DDSDDE(NTENS,NTENS),
     1 DDSDDT(NTENS),DRPLDE(NTENS),STRAN(NTENS),DSTRAN(NTENS),
     2 TIME(2),PREDEF(1),DPRED(1),PROPS(NPROPS),COORDS(3),DROT(3,3),
     3 DFGRD0(3,3),DFGRD1(3,3)
	 
      do k1=1,ntens
       do k2=1,ntens
        DDSDDE(k1,k2)=0.0d0
       enddo
      enddo
     	   
	    STATEV(1)=strain11out(npt,noel-elemshift)
	    STATEV(2)=strain22out(npt,noel-elemshift)
	    STATEV(3)=strain12out(npt,noel-elemshift)
	    STATEV(4)=stress11out(npt,noel-elemshift)
	    STATEV(5)=stress22out(npt,noel-elemshift)
	    STATEV(6)=stress12out(npt,noel-elemshift)   
	  
	   if (elemout(noel-elemshift).ne.noel-elemshift) then 
	        call xit
	   end if
            
      RETURN
      END