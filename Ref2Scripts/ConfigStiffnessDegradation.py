import LaminateModel
import utilities
import math
import numpy as np

# geometrical definitions
dr = 0.3
rhoH = 0.5
theta = -60.

thetaB=math.pi/180.*theta #radians

# mesh
nle=1e-1 # characteristic element length

# loading 
Ni = np.array([1e5,0,0]) # [N1, N3, N13] 

# material properties
mat = utilities.material(E1=30620., E2=8620., E3=8620.,
                         nu23=0.33  , nu13=0.29	, nu12=0.29,
                         G23=2900., G13=3250., G12=3250.)
# mat = utilities.material(E1=266000., E2=5490., E3=5490.,
                         # nu23=0.4, nu13=0.27, nu12=0.27,
                         # G23=2370., G13=3540., G12=3540.)
                         
# layup definition
layup_crack = np.array([1,0,0,0])
layup_delaminationcrack = np.array([0,1,0,0])
layup_angles = np.array([thetaB,0.,-thetaB,0.]) 
layup_heights = np.array([1.,1.,1.,1.])*0.5 

# microstructure definition
mic = utilities.microstructure(layup_microstructure=np.zeros(len(layup_crack)))

# abaqus model parameters
UMAT = True 
contact = True

NameModel='CompModel'
NameJob='CompJob'
NamePart='Laminate'
NameInstance='Laminate_0'

# classical laminate theory calculation for the normalization
layup_active = np.ones(len(layup_crack))
eps, sig_0, sig_0_l = utilities.CLT(layup_angles, layup_heights, mat, Ni, layup_active)
K1_CLT = Ni[0] / np.sum(layup_heights) / eps[0]
K3_CLT = Ni[1] / np.sum(layup_heights) / eps[1]

# ply discount as lower estimate of stiffness drop
layup_active = layup_active - layup_crack
eps_pd, sig_0_pd, sig_0_l_pd = utilities.CLT(layup_angles, layup_heights, mat, Ni, layup_active)
K1_CLT_pd = Ni[0]/ np.sum(layup_heights) / eps_pd[0]

# create the model
job, NameJob, nle_adjusted, num_nodes   = LaminateModel.BuildLaminateModel(dr, rhoH, thetaB, mat, Ni, 
                                                                           layup_crack, layup_delaminationcrack,
                                                                           layup_angles, layup_heights,
                                                                           nle, UMAT, mic, 
                                                                           contact=contact)
    
mdb.jobs[NameJob].submit(consistencyChecking=OFF)
job.waitForCompletion()

# postprocessing 
if Ni[0] != 0:
    K1, contact = LaminateModel.ExtractAxialStiffness(Ni, rhoH, thetaB, layup_heights, contact=contact)
    K1_norm = K1 / K1_CLT
    
    np.savetxt("K1_norm.txt",[K1_norm],delimiter=', ') #outputs the normalized stiffness
    np.savetxt("Contact.txt",[contact],delimiter=', ') #outputs the relative number of nodes where a change in sign occurs in either the overlap or the reactions forces (between 0 and 1)