# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2019 replay file
# Internal Version: 2018_09_24-20.41.51 157541
# Run by leohe on Sun Dec  6 11:32:19 2020
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=233.0, 
    height=226.333343505859)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='D45_W40_Hh2_L140_G004_001_T.odb')
#* OdbError: The database is from a previous release of Abaqus. 
#* Run abaqus -upgrade -job <newFileName> -odb <oldOdbFileName> to upgrade it.
from  abaqus import session
session.upgradeOdb("M:/Paper2020/D45_W40_Hh2_L140_G004_001_T.odb", 
    "C:/Users/leohe/AppData/Local/Temp/6/D45_W40_Hh2_L140_G004_001_T1607250748.856.odb", 
    )
from  abaqus import session
o3 = session.openOdb(
    'C:/Users/leohe/AppData/Local/Temp/6/D45_W40_Hh2_L140_G004_001_T1607250748.856.odb')
#: Model: C:/Users/leohe/AppData/Local/Temp/6/D45_W40_Hh2_L140_G004_001_T1607250748.856.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     2
#: Number of Meshes:             3
#: Number of Element Sets:       18
#: Number of Node Sets:          27
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 1'].odbDisplay.display.setValues(
    plotState=CONTOURS_ON_DEF)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U3'), )
session.viewports['Viewport: 1'].odbDisplay.setValues(viewCutNames=('Z-Plane', 
    ), viewCut=ON)
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    visibleEdges=FEATURE)
session.viewports['Viewport: 1'].view.setValues(nearPlane=308.316, 
    farPlane=349.468, width=29.5035, height=11.6321, viewOffsetX=2.63055, 
    viewOffsetY=0.103158)
session.viewports['Viewport: 1'].view.setValues(nearPlane=309.269, 
    farPlane=348.515, width=15.7666, height=6.21617, viewOffsetX=0.67247, 
    viewOffsetY=0.666572)
