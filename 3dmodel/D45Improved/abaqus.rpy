# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2018 replay file
# Internal Version: 2017_11_07-18.21.41 127140
# Run by s164380 on Tue Apr 23 13:37:46 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=261.333343505859, 
    height=222.0)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='Kmodel0MeshB01.odb')
#* OdbError: Z:/SimonModel/D45Improved/Kmodel0MeshB01.odb is from a more recent 
#* release of Abaqus.
#* 
#*  The current Abaqus installation must be upgraded before this output 
#* database can be opened.
