########################################################
## Run without GUI with following command 
#     abaqus cae noGUI=AbaRun.py
#########################################################

## Default ABAQUS inputs
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *

## Model in names
NameCAE='Kmodel0MeshB01.cae'
UserRoutine=''
NameModelin='Kvalue'
NameModel=  'Kmodel0MeshB01'
Ncpus=200

## Import model from other cae-file
mdb.openAuxMdb(pathName=NameCAE)
mdb.copyAuxMdbModel(fromName=NameModelin, toName=NameModel)
mdb.closeAuxMdb()

## Eventually Modify model

## Generate job
modelJob=mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
	explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
	memory=90, memoryUnits=PERCENTAGE, model=NameModel, modelPrint=OFF, 
	multiprocessingMode=MPI, name=NameModel, nodalOutputPrecision=SINGLE, 
	numCpus=Ncpus, numDomains=Ncpus, numGPUs=0, queue=None, scratch='', type=ANALYSIS, 
	userSubroutine=UserRoutine, waitHours=0, waitMinutes=0)

## The following lines submit the job, and wait for completion before continue  
modelJob.submit(consistencyChecking=OFF)
modelJob.waitForCompletion()
	
## Eventually Post-processing