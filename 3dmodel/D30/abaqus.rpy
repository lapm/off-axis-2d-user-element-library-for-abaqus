# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2018 replay file
# Internal Version: 2017_11_07-18.21.41 127140
# Run by s164380 on Tue May 14 15:19:49 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=336.53515625, 
    height=355.0)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='D30_W180_Hh2_L140_G004_001_T_5.odb')
#* OdbError: The database is from a previous release of Abaqus. 
#* Run abaqus -upgrade -job <newFileName> -odb <oldOdbFileName> to upgrade it.
from  abaqus import session
session.upgradeOdb("Z:/SimonModel/D30/D30_W180_Hh2_L140_G004_001_T_5.odb", 
    "C:/Users/s164380/AppData/Local/Temp/102/D30_W180_Hh2_L140_G004_001_T_51557839995.146.odb", 
    )
from  abaqus import session
o3 = session.openOdb(
    'C:/Users/s164380/AppData/Local/Temp/102/D30_W180_Hh2_L140_G004_001_T_51557839995.146.odb')
#: Model: C:/Users/s164380/AppData/Local/Temp/102/D30_W180_Hh2_L140_G004_001_T_51557839995.146.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     2
#: Number of Meshes:             3
#: Number of Element Sets:       25
#: Number of Node Sets:          39
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    visibleEdges=FEATURE)
