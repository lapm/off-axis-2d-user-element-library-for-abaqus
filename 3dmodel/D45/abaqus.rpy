# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2018 replay file
# Internal Version: 2017_11_07-18.21.41 127140
# Run by s164380 on Tue May 21 13:18:12 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=336.53515625, 
    height=333.333343505859)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='D45_W40_Hh2_L140_G004_001_T.odb')
#* OdbError: The database is from a previous release of Abaqus. 
#* Run abaqus -upgrade -job <newFileName> -odb <oldOdbFileName> to upgrade it.
from  abaqus import session
session.upgradeOdb("Z:/SimonModel/D45/D45_W40_Hh2_L140_G004_001_T.odb", 
    "C:/Users/s164380/AppData/Local/Temp/116/D45_W40_Hh2_L140_G004_001_T1558437498.021.odb", 
    )
from  abaqus import session
o3 = session.openOdb(
    'C:/Users/s164380/AppData/Local/Temp/116/D45_W40_Hh2_L140_G004_001_T1558437498.021.odb')
#: Model: C:/Users/s164380/AppData/Local/Temp/116/D45_W40_Hh2_L140_G004_001_T1558437498.021.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     2
#: Number of Meshes:             3
#: Number of Element Sets:       18
#: Number of Node Sets:          27
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.setValues(viewCutNames=('Z-Plane', 
    ), viewCut=ON)
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    visibleEdges=FEATURE)
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 1'].view.setValues(nearPlane=237.493, 
    farPlane=308.246, width=154.62, height=139.732, viewOffsetX=1.37521, 
    viewOffsetY=3.64994)
session.viewports['Viewport: 1'].view.setValues(nearPlane=252.267, 
    farPlane=293.473, width=18.7155, height=16.9135, viewOffsetX=-1.38581, 
    viewOffsetY=-1.4732)
session.viewports['Viewport: 1'].view.setValues(nearPlane=253.523, 
    farPlane=292.216, width=7.0642, height=6.38403, viewOffsetX=-1.96567, 
    viewOffsetY=0.130295)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U3'), )
session.viewports['Viewport: 1'].view.setValues(nearPlane=253.213, 
    farPlane=292.527, width=10.2274, height=9.24264, viewOffsetX=-1.93762, 
    viewOffsetY=-0.578114)
session.viewports['Viewport: 1'].view.setValues(session.views['Right'])
session.viewports['Viewport: 1'].view.setValues(session.views['Left'])
session.viewports['Viewport: 1'].view.setValues(session.views['User-1'])
session.viewports['Viewport: 1'].view.setValues(session.views['Iso'])
session.viewports['Viewport: 1'].view.setValues(nearPlane=298.987, 
    farPlane=428.726, width=34.7405, height=31.3955, viewOffsetX=-11.2382, 
    viewOffsetY=0.599859)
session.viewports['Viewport: 1'].odbDisplay.setValues(viewCutNames=('Z-Plane', 
    ), viewCut=OFF)
#: 
#: Node: PART-1.243624
#:                                         1             2             3        Magnitude
#: Base coordinates:                  6.25004e+00,  4.00000e+00,  2.89079e+01,      -      
#: Scale:                             2.33333e+01,  2.33333e+01,  2.33333e+01,      -      
#: Deformed coordinates (unscaled):   6.21670e+00,  4.00000e+00,  2.88766e+01,      -      
#: Deformed coordinates (scaled):     5.47207e+00,  4.00000e+00,  2.81772e+01,      -      
#: Displacement (unscaled):          -3.33415e-02, -1.61992e-37, -3.13173e-02,  4.57431e-02
#: 
#: Node: PART-1.157715
#:                                         1             2             3        Magnitude
#: Base coordinates:                  2.00362e+01,  4.00000e+00,  3.34111e+01,      -      
#: Scale:                             2.33333e+01,  2.33333e+01,  2.33333e+01,      -      
#: Deformed coordinates (unscaled):   2.01193e+01,  4.00000e+00,  3.33624e+01,      -      
#: Deformed coordinates (scaled):     2.19754e+01,  4.00000e+00,  3.22765e+01,      -      
#: Displacement (unscaled):           8.31083e-02, -8.57742e-38, -4.86247e-02,  9.62879e-02
session.Path(name='Path-1', type=POINT_LIST, expression=((0.0, 4.0, 20.0), (
    20.0, 4.0, 20.0)))
pth = session.paths['Path-1']
session.XYDataFromPath(name='U3', path=pth, includeIntersections=True, 
    projectOntoMesh=False, pathStyle=PATH_POINTS, numIntervals=10, 
    projectionTolerance=0, shape=UNDEFORMED, labelType=TRUE_DISTANCE)
xyp = session.XYPlot('XYPlot-1')
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
xy1 = session.xyDataObjects['U3']
c1 = session.Curve(xyData=xy1)
chart.setValues(curvesToPlot=(c1, ), )
session.viewports['Viewport: 1'].setValues(displayedObject=xyp)
odb = session.odbs['C:/Users/s164380/AppData/Local/Temp/116/D45_W40_Hh2_L140_G004_001_T1558437498.021.odb']
session.viewports['Viewport: 1'].setValues(displayedObject=odb)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    DEFORMED, ))
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
del session.xyDataObjects['U3']
pth = session.paths['Path-1']
session.XYDataFromPath(name='U3', path=pth, includeIntersections=True, 
    projectOntoMesh=False, pathStyle=PATH_POINTS, numIntervals=10, 
    projectionTolerance=0, shape=UNDEFORMED, labelType=X_COORDINATE)
xyp = session.xyPlots['XYPlot-1']
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
xy1 = session.xyDataObjects['U3']
c1 = session.Curve(xyData=xy1)
chart.setValues(curvesToPlot=(c1, ), )
session.viewports['Viewport: 1'].setValues(displayedObject=xyp)
odb = session.odbs['C:/Users/s164380/AppData/Local/Temp/116/D45_W40_Hh2_L140_G004_001_T1558437498.021.odb']
session.viewports['Viewport: 1'].setValues(displayedObject=odb)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    DEFORMED, ))
session.viewports['Viewport: 1'].view.setValues(nearPlane=297.399, 
    farPlane=430.314, width=53.2877, height=48.1569, viewOffsetX=-14.8973, 
    viewOffsetY=-3.34012)
xyp = session.xyPlots['XYPlot-1']
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
xy1 = session.xyDataObjects['U3']
c1 = session.Curve(xyData=xy1)
chart.setValues(curvesToPlot=(c1, ), )
session.viewports['Viewport: 1'].setValues(displayedObject=xyp)
