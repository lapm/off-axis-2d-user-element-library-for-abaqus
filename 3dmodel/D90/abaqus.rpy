# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2018 replay file
# Internal Version: 2017_11_07-18.21.41 127140
# Run by s164380 on Wed Apr 24 09:05:21 2019
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=261.333343505859, 
    height=222.0)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='D90_W40_Hh2_L140_G004_001_T.odb')
#* OdbError: The database is from a previous release of Abaqus. 
#* Run abaqus -upgrade -job <newFileName> -odb <oldOdbFileName> to upgrade it.
from  abaqus import session
session.upgradeOdb("Z:/SimonModel/D90/D90_W40_Hh2_L140_G004_001_T.odb", 
    "C:/Users/s164380/AppData/Local/Temp/12/D90_W40_Hh2_L140_G004_001_T1556089529.01.odb", 
    )
from  abaqus import session
o3 = session.openOdb(
    'C:/Users/s164380/AppData/Local/Temp/12/D90_W40_Hh2_L140_G004_001_T1556089529.01.odb')
#: Model: C:/Users/s164380/AppData/Local/Temp/12/D90_W40_Hh2_L140_G004_001_T1556089529.01.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     2
#: Number of Meshes:             3
#: Number of Element Sets:       17
#: Number of Node Sets:          28
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.setValues(viewCutNames=('Z-Plane', 
    ), viewCut=ON)
session.viewports['Viewport: 1'].view.setValues(session.views['Front'])
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    visibleEdges=FEATURE)
odb = session.odbs['C:/Users/s164380/AppData/Local/Temp/12/D90_W40_Hh2_L140_G004_001_T1556089529.01.odb']
scratchOdb = session.ScratchOdb(odb)
scratchOdb.rootAssembly.DatumCsysByThreePoints(name='CSYS-1', 
    coordSysType=CARTESIAN, origin=(0.0, 0.0, 0.0), point1=(1.0, 0.0, 0.0), 
    point2=(0.0, 1.0, 0.0))
dtm = session.scratchOdbs['C:/Users/s164380/AppData/Local/Temp/12/D90_W40_Hh2_L140_G004_001_T1556089529.01.odb'].rootAssembly.datumCsyses['CSYS-1']
session.viewports['Viewport: 1'].odbDisplay.basicOptions.setValues(
    transformationType=USER_SPECIFIED, datumCsys=dtm)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_UNDEF, ))
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(COMPONENT, 
    'S11'), )
session.viewports['Viewport: 1'].view.setValues(nearPlane=249.975, 
    farPlane=295.937, width=19.6418, height=16.5035, viewOffsetX=-57.7621, 
    viewOffsetY=-1.76161)
