#initialization of packages
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *
import fileinput
import sys
import numpy as np
import string
####################################define the model parameters
theta=45.
h=1.
l=70.
t=1. 
w=2.0*h
botline=-3*h
disp=0.6 		#displacement applied to the left edge

#material properties GFRP1
E1=30620. 
nu23=0.3261    
G23=2900.
E2=8620. 
nu13=0.2858	
G13=3250.
E3=8620.
nu12=0.2858
G12=3250.

thetamat=1.5707963267949/90.*theta  		#radians
thetaB=1.5707963267949/90.*theta			#radians

#characteristic element length
nle=0.01

NameModel='CompModel'
NameJob='CompJob'
NamePart='Laminate'
NameInstance='Laminate_0'

####################################Classical laminate theory calculation

#Plane stress constitutive matrix
Qli=np.matrix([[1/E1,-nu12/E1,0],[-nu12/E1,1/E2,0],[0,0,1/G12]]) 
Ql=np.linalg.inv(Qli)

#Transformation matrices
c=cos(-thetamat)
s=sin(-thetamat)
T1=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])
c=cos(0)
s=sin(0)
T2=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])
T4=T2
c=cos(thetamat)
s=sin(thetamat)
T3=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])

#Global constitutive matrices
Q1=T1*Ql*np.transpose(T1)
Q2=T2*Ql*np.transpose(T2)
Q3=T3*Ql*np.transpose(T3)
Q4=Q2

#Extensional stiffness matrix
A=Q1*h+Q2*h+Q3*h+Q4*h
Ainv=np.linalg.inv(A)

#Find strains and stresses
e110=disp/l
Nx=e110/Ainv[0,0]
e330=Nx*Ainv[1,0]
e130=Nx*Ainv[2,0]
ee=np.array([e110,e330,e130])
ss=np.dot(Q1,ee)
sigmax=ss[0,0]
ssl=np.dot(Ql,np.asarray(np.dot(np.transpose(T1),ee))[0])
sigma13=-(ssl[0,2])
sigma11=(ssl[0,1])
gamma120=0.
gamma130=0.
gamma230=0.

####################################model of the loading situation in ABAQUS
#model the geometry
modelObj = mdb.Model(name=NameModel)

s1 = modelObj.ConstrainedSketch(name='__profile__', 
    sheetSize=200.0)
g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
s1.setPrimaryObject(option=STANDALONE)
s1.rectangle(point1=(0.0, botline), point2=(l, h))
p = modelObj.Part(name=NamePart, dimensionality=THREE_D, 
    type=DEFORMABLE_BODY)
partObj = modelObj.parts[NamePart]	
partObj.BaseShell(sketch=s1)
s1.unsetPrimaryObject() 

#partObj = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=partObj)
del modelObj.sketches['__profile__']
#p = mdb.models['Model-1'].parts['Part-1']
f1, e1, d2 = partObj.faces, partObj.edges, partObj.datums

t = partObj.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, 
    0.0, 0.0), normal=(0.0, 0.0, 1.0)), sketchPlaneSide=SIDE1, 
    origin=(0.0, 0.0, 0.0))
s = modelObj.ConstrainedSketch(name='__profile__', 
    sheetSize=10.19, gridSpacing=0.25, transform=t)
	
g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
s.setPrimaryObject(option=SUPERIMPOSE)

partObj.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

s.Line(point1=(0.0, 0.0), point2=(l, 0.0))
s.Line(point1=(0.0, -h), point2=(l, -h))
s.Line(point1=(0.0, -2.0*h), point2=(l, -2.0*h))


f = partObj.faces
pickedFaces = f.findAt(((0.0, 0.0, 0.0), ))
e, d1 = partObj.edges, partObj.datums
partObj.PartitionFaceBySketch(faces=pickedFaces, sketch=s)
s.unsetPrimaryObject()
del modelObj.sketches['__profile__']

#define the sets 
#face sets
f=partObj.faces
for i in range(4):
    faces=f.findAt(((0.5*l,(-2.5+i)*h,0.),))
    partObj.Set(faces=faces, name='Laminate'+str(i+1))

#edge sets
e=partObj.edges
edges=e.findAt(((0.5*l,botline,0.),))
partObj.Set(edges=edges, name='bot')

edges=e.findAt(((l,-2.5*h,0.), ),((l,-1.5*h,0.), ), ((l,-0.5*h,0.), ),((l,-0.5*h,0.),))
partObj.Set(edges=edges, name='right')

edges=e.findAt(((0.5*l,h,0.),))
partObj.Set(edges=edges, name='top')

edges=e.findAt(((0.0, -2.5*h, 0.0), ), ((0.0, -1.5*h, 0.0), ), ((0.0,-0.5*h,0.), ), \
((0.0,0.5*h,0.), ))
partObj.Set(edges=edges, name='left')

#vertex sets
v=partObj.vertices
verts=v.findAt(((0.,1.,0.),))
partObj.Set(vertices=verts, name='origin')
	
#create step 1
modelObj.StaticStep(name='Step-1', previous='Initial')

#create instance
modelObj.rootAssembly.DatumCsysByDefault(CARTESIAN)
instanceObj = modelObj.rootAssembly.Instance(dependent=ON, name=NameInstance, 
    part=partObj)
	
#apply boundary conditions
modelObj.DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
    distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    'BC-3', region=instanceObj.sets['left'], u1=
    -disp, u2=UNSET,u3=0.0,ur1=UNSET,ur2=UNSET,ur3=UNSET)
modelObj.DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
    distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    'BC-2', region=instanceObj.sets['top'], u1=
    UNSET, u2=0.0,u3=UNSET,ur1=UNSET,ur2=UNSET,ur3=UNSET)
modelObj.DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
    distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    'BC-4', region=instanceObj.sets['right'], u1=
    0.0, u2=UNSET,u3=0.0,ur1=UNSET,ur2=UNSET,ur3=UNSET)
	
#mesh
partObj.setElementType(elemTypes=(ElemType(
    elemCode=M3D8, elemLibrary=STANDARD), ElemType(elemCode=M3D6, 
    elemLibrary=STANDARD)), regions=(
    partObj.faces.findAt(((0.5*l,0.5*h,0.),),((0.5*l,-0.5*h,0.),),
	((0.5*l,-1.5*h,0.),), ((0.5*l,-2.5*h,0.),)),))
	
LEsizetop=400
seedside=[200,100,20,1]

#top of layers
for i in range(4):
    partObj.seedEdgeByBias(biasMethod=SINGLE,
        constraint=FINER, end2Edges=
        e.findAt(((0.5*l,botline+(i+1)*h,0.),)),
        maxSize=nle*LEsizetop, minSize=nle)
#bot 
partObj.seedEdgeByBias(biasMethod=SINGLE, 
    constraint=FINER, end1Edges=
    e.findAt(((0.5*l,botline,0.),)),
    maxSize=nle*LEsizetop, minSize=nle)  

#right
for i in range(3):
    partObj.seedEdgeByBias(biasMethod=SINGLE, 
        constraint=FINER, end1Edges=
        e.findAt(((l,botline+h*(i+0.5),0.),)),
        maxSize=nle*seedside[i], minSize=nle*seedside[i+1])
partObj.seedEdgeByBias(biasMethod=SINGLE, 
        constraint=FINER, end2Edges=
        e.findAt(((l,0.5*h,0.),)),
        maxSize=nle*seedside[2], minSize=nle*seedside[3])
#left
for i in range(3):
    partObj.seedEdgeByBias(biasMethod=SINGLE, 
        constraint=FINER, end2Edges=
        e.findAt(((0.,botline+h*(i+0.5),0.),)),
        maxSize=nle*seedside[i], minSize=nle*seedside[i+1])
partObj.seedEdgeByBias(biasMethod=SINGLE, 
        constraint=FINER, end1Edges=
        e.findAt(((0.,0.5*h,0.),)),
        maxSize=nle*seedside[2], minSize=nle*seedside[3])
	
#mesh controls	
partObj.setMeshControls(regions=
    partObj.faces.findAt(((0.0, h*0.75, 
    0.0), ), ((0.0, h*0.25, 0.0), ), ), technique=FREE)

partObj.setMeshControls(elemShape=QUAD, regions=
    partObj.faces.findAt(((0.0, h*0.75, 
    0.0), ), ((0.0, h*0.25, 0.0), ), ))
	
partObj.generateMesh()
	
#create job and the input file 
mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
    explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
    memory=90, memoryUnits=PERCENTAGE, model=NameModel, modelPrint=OFF, 
    multiprocessingMode=DEFAULT, name=NameJob, nodalOutputPrecision=SINGLE, 
    numCpus=1, numGPUs=0, queue=None, resultsFormat=ODB, scratch='', type=
    ANALYSIS, userSubroutine=
    'OA2D.f', waitHours=0, 
    waitMinutes=0)

#write input file
mdb.jobs[NameJob].writeInput(consistencyChecking=OFF)

####################################modify input file for UEL
for line in fileinput.FileInput(NameJob+".inp",inplace=1):
    if "*Element, type=M3D8" in line:
        line=line.replace(line,"*USER ELEMENT, NODES=8, TYPE=U1, PROPERTIES=13, \
COORDINATES=3, VARIABLES=24 \n1,2,3 \n*Element, type=U1 \n")
    print line,

#write copy of elements
test=open(NameJob+".inp","r")
linetop=0
linebottom=0
for i,line in enumerate(test,1):
	if "*Element, type=U1" in line:
		linetop=i
	elif "*Nset, nset=Laminate1" in line:	
		linebottom=i
		break
test.close()

lines=open(NameJob+".inp").readlines()
open("elementcopy.txt",'w').writelines(lines[linetop:linebottom-1])	
from numpy import genfromtxt
matricentemp=genfromtxt("elementcopy.txt", delimiter=',')
matricentemp[:,0]=matricentemp[:,0]+1000000 				#max number of nodes
matricentemp=matricentemp.astype(int)

np.set_printoptions(threshold=100000000000000)
temp=np.array2string(matricentemp,separator=',')
temp = temp.translate(string.maketrans('', ''), '[')
temp = temp.translate(string.maketrans('', ''), ']')
temp="*Element, type=M3D8\n"+temp+"\n*Nset, nset=Laminate1 \n"
for line in fileinput.FileInput(NameJob+".inp",inplace=1):
	if "*Nset, nset=Laminate1" in line:
		line=line.replace(line,temp)
	print line,

temp=np.array2string(matricentemp[:,0],separator=',')
temp = temp.translate(string.maketrans('', ''), '[')
temp = temp.translate(string.maketrans('', ''), ']')
	
stringstart1="*Elset, elset=UMatOut\n"
stringstart=stringstart1+temp +"\n"
	
#creating string for UEL replacement line
stringuel1="*UEL PROPERTY, ELSET=Laminate1 \n"
stringuel2=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
str(G12) + ", " + "0.0" + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
stringuel3=" \n*UEL PROPERTY, ELSET=Laminate2 \n"
stringuel4=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
str(G12) + ", " + str(thetamat) + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
stringuel5=" \n*UEL PROPERTY, ELSET=Laminate3 \n"
stringuel6=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
str(G12) + ", " +"0.0" + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
stringuel7=" \n*UEL PROPERTY, ELSET=Laminate4 \n"
stringuel8=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
str(G12) + ", " + str(-thetamat) + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)

stringuelumat=" \n*Membrane Section, elset=UMatOut, material=UMatOutMat \n1.,"
stringuel10=" \n*End Part \n"
stringuel=stringstart+stringuel1+stringuel2+stringuel3+stringuel4+stringuel5+stringuel6+\
stringuel7+stringuel8+stringuelumat+stringuel10
	
for line in fileinput.FileInput(NameJob+".inp",inplace=1):
    if "*End Part" in line:
        line=line.replace(line,stringuel)
    print line,
	
stringumat2="""*End Assembly
**  THE LINES BELOW WERE EDITED TO DEFINE THE USER MATERIAL 
*Material, name=UMatOutMat
*user material, constants=1, type=mechanical
0
** This defines the number of state variables
*DEPVAR
12
"""
for line in fileinput.FileInput(NameJob+".inp",inplace=1):
	if "*End Assembly" in line:
		line=line.replace(line,stringumat2)
	print line,

counter = 0
for line in fileinput.input(NameJob+'.inp', inplace=True):
    if not counter:
        if line.startswith('** OUTPUT REQUESTS'):
            counter = 100
        else:
            print line,
    else:
        counter -= 1
		

test=open(NameJob+".inp","a+")
test.write("""** OUTPUT REQUESTS
** 
*Restart, write, frequency=0
** 
** FIELD OUTPUT: F-Output-1
** 
*Output, field
*Node Output
U, 
*Element Output, directions=YES
E, S, SDV
** 
** The lines below request output to be printed to the .dat file
**
** SDV are the element state variables
**
*el print, freq=1
SDV
*node print, freq=1 
COORD,U,RF
**
**  The lines below request data to be printed to .fil output
**
**  These data can be read for post-processing
**
*FILE FORMAT, ASCII
*EL FILE
,
SDV
*NODE FILE
COORD,U,RF
*End Step""")
test.close()


####################################submit the job
realjob = mdb.JobFromInputFile(name=NameJob,
	inputFileName=NameJob+'.inp', 
	type=ANALYSIS, atTime=None, waitMinutes=0, waitHours=0, queue=None, 
	memory=90, memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
	explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, 
	userSubroutine='OA2D.f',  
	scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=1, 
	numGPUs=0)
mdb.jobs[NameJob].submit(consistencyChecking=OFF)

#waiting
realjob.waitForCompletion()

####################################extract displacements U
#find result table in .dat file
test2=open(NameJob+".dat","r")
linetop=0
linebottom=0
for i,line in enumerate(test2,1):
	if "THE FOLLOWING TABLE IS PRINTED FOR ALL NODES" in line:
		linetop=i
	elif "THE ANALYSIS HAS BEEN COMPLETED" in line:	
		linebottom=i
		break
test2.close()

lines=open(NameJob+".dat").readlines()
open("data_behandling.txt",'w').writelines(lines[linetop+4:linebottom-9])	

matricen=np.loadtxt("data_behandling.txt")
NodeNr=matricen[:,0]
COOR1=matricen[:,1]
COOR2=matricen[:,2]
U1=matricen[:,4]
U2=matricen[:,5]
U3=matricen[:,6]
RF1=matricen[:,7]
RF2=matricen[:,8]
RF3=matricen[:,9]

#modification of the displacements
index=COOR1>l-nle/3
COOR2=COOR2[index]
U1=U1[index]
U2=U2[index]
U3=U3[index]

index=COOR2>-nle/6 
COOR2=COOR2[index]
U1=U1[index]
U2=U2[index]
U3=U3[index]

#sort
arg=np.argsort(COOR2)
COOR2=COOR2[arg]
U1=U1[arg]
U2=U2[arg]
U3=U3[arg]

#find normal and translational displacement
UN=(U1*sin(thetaB)-U3*cos(thetaB))*2	
UT=(U1*cos(thetaB)+U3*sin(thetaB))*2

####################################Calculation of the energy release rate

#Integration with Simpson's Rule
sim_areas=np.zeros((len(COOR2)-1.0)/2.0)
for i in range(0,len(COOR2)-1,2):
	sim_areas[i/2]=COOR2[i+2]-COOR2[i]
Gss11simp=0.0
Gss13simp=0.0
for i in range(int((len(COOR2)-1.0)/2.0)):
	Gss11simp=(Gss11simp+1.0/6.0*sim_areas[i]*( (abs(UN[2*i])+4.0*abs(UN[2*i+1])+\
abs(UN[2*i+2]))*sigma11)/(h*2.0))
	Gss13simp=(Gss13simp+1.0/6.0*sim_areas[i]*((abs(UT[2*i])+4.0*abs(UT[2*i+1])+\
abs(UT[2*i+2]))*sigma13  )/(h*2.0))
Gsssimp=(Gss11simp+Gss13simp)

print("Gss_simpsons",Gsssimp)
print("normalized11_simpsons",Gss11simp/e110/h/sigmax/2)
print("normalized13_simspons",Gss13simp/e110/h/sigmax/2)
print("normalized_simpsons",Gsssimp/e110/h/sigmax/2)

np.savetxt('Gss.txt',[Gss11simp/e110/h/sigmax/2, Gss13simp/e110/h/sigmax/2, Gsssimp/e110/h/sigmax/2],delimiter=', ')
np.savetxt('PrescribedStrains.txt',[e110,gamma120,gamma130,gamma230,e330],delimiter=', ')
	
print("completed")
