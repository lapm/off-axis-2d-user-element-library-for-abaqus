# -*- coding: mbcs -*-
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *
import fileinput
import sys
import numpy as np
import string
from shutil import copyfile


StrMat='G1'
Degree=45

#listt=np.array([1.,1.1,1.3,1.5,1.7,2.,2.5,3.,4.,5.,6.,7.5,10.])
listt=np.array([5.])
NameOutput=StrMat+'T'+str(Degree)+'F'

totaliii=len(listt)

listGss11=np.zeros((1,totaliii))
listGss13=np.zeros((1,totaliii))
listlh=np.zeros((1,totaliii))
for iii in range(totaliii): 
    print(iii);
    ####################################defining the model parameters
    h=1.
    
    l=350.
    t=1. 
    w=2.0*h
    botline=-3*h
    disp=0.6 		#displacement applied to the left edge
    #material properties GFRP1
    if StrMat=='G1':   #material properties GFRP1
        E1=30620. 
        nu23=0.3261    
        G23=2900.
        E2=8620. 
        nu13=0.2858	
        G13=3250.
        E3=8620.
        nu12=0.2858
        G12=3250.
    elif StrMat=='G2':   #material properties GFRP2
        E1=41200. 
        nu23=0.41    
        G23=3660.
        E2=10300. 
        nu13=0.26	
        G13=3790.
        E3=10300.
        nu12=0.26
        G12=3790.
    elif StrMat=='C1': #material properties CFRP1
        E1=126000. 
        nu23=0.40    
        G23=2700.
        E2=7560. 
        nu13=0.26	
        G13=3690.
        E3=7560.
        nu12=0.26
        G12=3690.
    elif StrMat=='C2': #material properties CFRP2
        E1=266000. 
        nu23=0.40    
        G23=2370.
        E2=5490. 
        nu13=0.27	
        G13=3540.
        E3=5490.
        nu12=0.27
        G12=3540.

    thetamat=Degree*1.5707963267949/90.		    #radians
    thetaB=Degree*1.5707963267949/90.			#radians
    
    
    lcrack=h*listt[iii]/sin(thetaB)

    #characteristic element length
    nle=0.01
    ####################################Classical laminate theory calculation

    #Plane stress constitutive matrix
    Qli=np.matrix([[1/E1,-nu12/E1,0],[-nu12/E1,1/E2,0],[0,0,1/G12]])
    Ql=np.linalg.inv(Qli)

    #Transformation matrices
    c=cos(-thetamat)
    s=sin(-thetamat)
    T1=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])
    c=cos(0)
    s=sin(0)
    T2=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])
    T4=T2
    c=cos(thetamat)
    s=sin(thetamat)
    T3=np.matrix([[c**2, s**2, 2*s*c], [s**2, c**2, -2*s*c], [-s*c, s*c, c**2-s**2]])

    #Global constitutive matrices
    Q1=T1*Ql*np.transpose(T1)
    Q2=T2*Ql*np.transpose(T2)
    Q3=T3*Ql*np.transpose(T3)
    Q4=Q2

    #Extensional stiffness matrix
    A=Q1*h+Q2*h+Q3*h+Q4*h
    Ainv=np.linalg.inv(A)

    #Find strains and stresses
    e110=disp/l
    Nx=e110/Ainv[0,0]
    e330=Nx*Ainv[1,0]
    e130=Nx*Ainv[2,0]
    ee=np.array([e110,e330,e130])
    ss=np.dot(Q1,ee)
    sigmax=ss[0,0]
    ssl=np.dot(Ql,np.asarray(np.dot(np.transpose(T1),ee))[0])
    sigma13=-(ssl[0,2])
    sigma11=(ssl[0,1])
    gamma120=0.
    gamma130=0.
    gamma230=0.

    
    listlock=np.array([0.5*h,-0.5*h])
    for iiii in range(2): 
        ####################################model of the loading situation in ABAQUS
        #model the geometry
        s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
            sheetSize=200.0)
        g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
        s1.setPrimaryObject(option=STANDALONE)
        s1.rectangle(point1=(0.0, botline), point2=(l, h))
        p = mdb.models['Model-1'].Part(name='Part-1', dimensionality=THREE_D, 
            type=DEFORMABLE_BODY)
        p = mdb.models['Model-1'].parts['Part-1']
        p.BaseShell(sketch=s1)
        s1.unsetPrimaryObject() 


        p = mdb.models['Model-1'].parts['Part-1']
        session.viewports['Viewport: 1'].setValues(displayedObject=p)
        del mdb.models['Model-1'].sketches['__profile__']
        p = mdb.models['Model-1'].parts['Part-1']
        f1, e1, d2 = p.faces, p.edges, p.datums

        t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, 
            0.0, 0.0), normal=(0.0, 0.0, 1.0)), sketchPlaneSide=SIDE1, 
            origin=(0.0, 0.0, 0.0))
        s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
            sheetSize=10.19, gridSpacing=0.25, transform=t)
            
        g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
        s.setPrimaryObject(option=SUPERIMPOSE)

        p = mdb.models['Model-1'].parts['Part-1']
        p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

        s.Line(point1=(0.0, 0.0), point2=(l, 0.0))
        s.Line(point1=(0.0, -h), point2=(l, -h))
        s.Line(point1=(0.0, -2.0*h), point2=(l, -2.0*h))

        p = mdb.models['Model-1'].parts['Part-1']
        f = p.faces
        pickedFaces = f.findAt(((0.0, 0.0, 0.0), ))
        e, d1 = p.edges, p.datums
        p.PartitionFaceBySketch(faces=pickedFaces, sketch=s)
        s.unsetPrimaryObject()
        del mdb.models['Model-1'].sketches['__profile__']


        #define the sets
        tempp=mdb.models['Model-1'].parts['Part-1']


        #face sets
        tempf=tempp.faces
        faces=tempf.findAt(((0.5*l,-2.5*h,0.),))
        tempp.Set(faces=faces, name='Laminate1')

        faces=tempf.findAt(((0.5*l,-1.5*h,0.),))
        tempp.Set(faces=faces, name='Laminate2')

        faces=tempf.findAt(((0.5*l,-0.5*h,0.),))
        tempp.Set(faces=faces, name='Laminate3')

        faces=tempf.findAt(((0.5*l,0.5*h,0.),))
        tempp.Set(faces=faces, name='Laminate4')		
                
        #edge sets
        tempe=tempp.edges
        edges=tempe.findAt(((0.5*l,botline,0.),))
        tempp.Set(edges=edges, name='bot')

        edges=tempe.findAt(((l,-2.5*h,0.), ),((l,-1.5*h,0.), ), ((l,-0.5*h,0.), ),((l,
        listlock[iiii],0.),))
        tempp.Set(edges=edges, name='right')

        edges=tempe.findAt(((0.5*l,h,0.),))
        tempp.Set(edges=edges, name='top')

        edges=tempe.findAt(((0.0, -2.5*h, 0.0), ), ((0.0, -1.5*h, 0.0), ), ((0.0,-0.5*h,0.), ), \
        ((0.0,0.5*h,0.), ))
        tempp.Set(edges=edges, name='left')

        #vertex sets
        tempv=tempp.vertices
        verts=tempv.findAt(((0.,1.,0.),))
        tempp.Set(vertices=verts, name='origin')
            
            
        #create crack 2 (partition)
        p = mdb.models['Model-1'].parts['Part-1']
        f1, e1, d2 = p.faces, p.edges, p.datums

        t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, 
            h*0.5, 0.0), normal=(0.0, 0.0, 1.0)), sketchPlaneSide=SIDE1, 
            origin=(0.0, 0.0, 0.0))
        s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
            sheetSize=10.19, gridSpacing=0.25, transform=t)
            
        g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
        s.setPrimaryObject(option=SUPERIMPOSE)

        p = mdb.models['Model-1'].parts['Part-1']
        p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)


        s.Line(point1=(l-lcrack,0.0),point2=(l-lcrack,h))
        p=mdb.models['Model-1'].parts['Part-1']
        f=p.faces
        pickedFaces=f.findAt(((l-lcrack, 0.5*h,0.0),))
        e1,d1=p.edges,p.datums
        p.PartitionFaceBySketch(sketchUpEdge=e1.findAt(coordinates=(l,0.5*h,0.0)),
            faces=pickedFaces,sketch=s)
        s.unsetPrimaryObject()
        del mdb.models['Model-1'].sketches['__profile__']

        #crack set
        #tempe=tempp.edges
        edges=tempe.findAt(((l-lcrack,0.5*h,0.),))
        tempp.Set(edges=edges, name='crack')

        #create crack partition meshing
        p = mdb.models['Model-1'].parts['Part-1']
        f1, e1, d2 = p.faces, p.edges, p.datums

        t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, 
            h*0.5, 0.0), normal=(0.0, 0.0, 1.0)), sketchPlaneSide=SIDE1, 
            origin=(0.0, 0.0, 0.0))
        s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
            sheetSize=10.19, gridSpacing=0.25, transform=t)
            
        g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
        s.setPrimaryObject(option=SUPERIMPOSE)

        p = mdb.models['Model-1'].parts['Part-1']
        p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)


        s.Line(point1=(l-lcrack,-3*h),point2=(l-lcrack,h))
        p=mdb.models['Model-1'].parts['Part-1']
        f=p.faces
        pickedFaces=f.findAt(((l-lcrack+0.1, 0.5*h,0.0),),((l-lcrack-0.1, 0.5*h,0.0),),((l-lcrack,-0.5*h,0.0),)
            ,((l-lcrack,-1.5*h,0.0),),((l-lcrack,-2.5*h,0.0),))
        e1,d1=p.edges,p.datums
        p.PartitionFaceBySketch(sketchUpEdge=e1.findAt(coordinates=(l,0.5*h,0.0)),
            faces=pickedFaces,sketch=s)
        s.unsetPrimaryObject()
        del mdb.models['Model-1'].sketches['__profile__']

        #crackmesh set
        #tempe=tempp.edges
        edges=tempe.findAt(((l-lcrack,-2.5*h,0.), ),((l-lcrack,-1.5*h,0.), ), ((l-lcrack,-0.5*h,0.), ),((l-lcrack,0.5*h,0.),))
        tempp.Set(edges=edges, name='crackmesh') 
                        
        #create step 1
        mdb.models['Model-1'].StaticStep(name='Step-1', previous='Initial')

        #create instance
        mdb.models['Model-1'].rootAssembly.DatumCsysByDefault(CARTESIAN)
        mdb.models['Model-1'].rootAssembly.Instance(dependent=ON, name='Part-1-1', 
            part=mdb.models['Model-1'].parts['Part-1'])
            
        #make crack (make independent)
        a1 = mdb.models['Model-1'].rootAssembly
        a1.makeIndependent(instances=(a1.instances['Part-1-1'], ))

        #make seam
        p = mdb.models['Model-1'].parts['Part-1']
        pickedRegions = p.sets['crack']
        mdb.models['Model-1'].parts['Part-1'].engineeringFeatures.assignSeam(
            regions=pickedRegions)
   
        #apply boundary conditions
        mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
            distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
            'BC-3', region=
            mdb.models['Model-1'].rootAssembly.instances['Part-1-1'].sets['left'], u1=
            -disp, u2=UNSET,u3=UNSET,ur1=UNSET,ur2=UNSET,ur3=UNSET)
        mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
            distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
            'BC-2', region=
            mdb.models['Model-1'].rootAssembly.instances['Part-1-1'].sets['top'], u1=
            UNSET, u2=0.0,u3=UNSET,ur1=UNSET,ur2=UNSET,ur3=UNSET)
        mdb.models['Model-1'].DisplacementBC(amplitude=UNSET, createStepName='Step-1', 
            distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
            'BC-4', region=
            mdb.models['Model-1'].rootAssembly.instances['Part-1-1'].sets['right'], u1=
            0.0, u2=UNSET,u3=0.0,ur1=UNSET,ur2=UNSET,ur3=UNSET)
            
        #mesh
        a = mdb.models['Model-1'].rootAssembly
        f1 = a.instances['Part-1-1'].faces
        mdb.models['Model-1'].rootAssembly.setElementType(elemTypes=(ElemType(
            elemCode=M3D8, elemLibrary=STANDARD), ElemType(elemCode=M3D6, 
            elemLibrary=STANDARD)), regions=(
            f1.findAt(((l-lcrack-0.1,0.5*h,0.),),((l-lcrack-0.1,-0.5*h,0.),),
            ((l-lcrack-0.1,-1.5*h,0.),), ((l-lcrack-0.1,-2.5*h,0.),),((l-lcrack+0.1,0.5*h,0.),),((l-lcrack+0.1,-0.5*h,0.),),
            ((l-lcrack+0.1,-1.5*h,0.),), ((l-lcrack+0.1,-2.5*h,0.),)),))
                
        LEsizetop=400
        seedside2=200
        seedside3=100
        seedside4=10
        #top of L4
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.1*l,h,0.),)),
            maxSize=nle*LEsizetop, minSize=nle)
                
        #top of L3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.1*l,0.0,0.),)),
            maxSize=nle*LEsizetop, minSize=nle)
                
        #top of L3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.1*l,-h,0.),)),
            maxSize=nle*LEsizetop, minSize=nle)
                
        #top of L3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.1*l,-2.0*h,0.),)),
            maxSize=nle*LEsizetop, minSize=nle)

        #bot
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((0.1*l,botline,0.),)),
            maxSize=nle*LEsizetop, minSize=nle)

        #right4
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((l,0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
                
        #right3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((l,-0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
                
        #right2
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((l,-1.5*h,0.),)),
            maxSize=nle*seedside3, minSize=nle*seedside4)
                
        #right1
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((l,-2.5*h,0.),)),
            maxSize=nle*seedside2, minSize=nle*seedside3)
                
        #left4
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((0.0,0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
            
        #left3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.0,-0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
                
        #left2
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.0,-1.5*h,0.),)),
            maxSize=nle*seedside3, minSize=nle*seedside4)
                
        #left1
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((0.0,-2.5*h,0.),)),
            maxSize=nle*seedside2, minSize=nle*seedside3)
            
        #crack4
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end1Edges=
            tempe.findAt(((l-lcrack,0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
            
        #crack3
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((l-lcrack,-0.5*h,0.),)),
            maxSize=nle*seedside4, minSize=nle)
                
        #crack2
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((l-lcrack,-1.5*h,0.),)),
            maxSize=nle*seedside3, minSize=nle*seedside4)
                
        #crack1
        mdb.models['Model-1'].rootAssembly.seedEdgeByBias(biasMethod=SINGLE, 
            constraint=FINER, end2Edges=
            tempe.findAt(((l-lcrack,-2.5*h,0.),)),
            maxSize=nle*seedside2, minSize=nle*seedside3)

            
        #rest (between two cracks)
        a = mdb.models['Model-1'].rootAssembly
        partInstances =(a.instances['Part-1-1'], )
        a.seedPartInstance(regions=partInstances, size=nle, deviationFactor=0.1, 
            minSizeFactor=0.1)

        #meshcontrols	
        mdb.models['Model-1'].rootAssembly.setMeshControls(regions=
            mdb.models['Model-1'].rootAssembly.instances['Part-1-1'].faces.findAt(((l-lcrack-0.1, h*0.75, 
            0.0), ), ((l-lcrack-0.1, h*0.25, 0.0), ), ((l-lcrack+0.1, h*0.25, 0.0), ), ((l-lcrack+0.1, h*0.75, 0.0), ), ), technique=FREE)

        mdb.models['Model-1'].rootAssembly.setMeshControls(elemShape=QUAD, regions=
            mdb.models['Model-1'].rootAssembly.instances['Part-1-1'].faces.findAt(((l-lcrack-0.1, h*0.75, 
            0.0), ), ((l-lcrack-0.1, h*0.25, 0.0), ), ((l-lcrack+0.1, h*0.75, 0.0), ), ((l-lcrack+0.1, h*0.25, 0.0), ), ))

        session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON, 
            optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
        session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
            meshTechnique=ON)
        a = mdb.models['Model-1'].rootAssembly
        a.regenerate()
        session.viewports['Viewport: 1'].setValues(displayedObject=a)
        partInstances =(a.instances['Part-1-1'], )
        a.generateMesh(regions=partInstances)

            
        #create field output
        mdb.models['Model-1'].FieldOutputRequest(name='F-Output-3', 
                        createStepName='Step-1', variables=('SDV', ))
            
        #create job and the input file 
        mdb.Job(atTime=None, contactPrint=OFF, description='', echoPrint=OFF, 
            explicitPrecision=SINGLE, getMemoryFromAnalysis=True, historyPrint=OFF, 
            memory=90, memoryUnits=PERCENTAGE, model='Model-1', modelPrint=OFF, 
            multiprocessingMode=DEFAULT, name='Job-1', nodalOutputPrecision=SINGLE, 
            numCpus=1, numGPUs=0, queue=None, resultsFormat=ODB, scratch='', type=
            ANALYSIS, userSubroutine=
            'OA2D.f', waitHours=0, 
            waitMinutes=0)

        #write input file
        mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)

        ####################################modify input file for UEL
        for line in fileinput.FileInput("Job-1.inp",inplace=1):
            if "*Element, type=M3D8" in line:
                line=line.replace(line,"*USER ELEMENT, NODES=8, TYPE=U1, PROPERTIES=13, \
        COORDINATES=3, VARIABLES=24 \n1,2,3 \n*Element, type=U1 \n")
            print line,

        #write copy of elements
        test=open("Job-1.inp","r")
        linetop=0
        linebottom=0
        for i,line in enumerate(test,1):
            if "*Element, type=U1" in line: 
                linetop=i
            elif "*Nset, nset=Laminate1" in line:	
                linebottom=i
                break
        test.close()

        lines=open("Job-1.inp").readlines()
        open("elementcopy.txt",'w').writelines(lines[linetop:linebottom-1])	
        from numpy import genfromtxt
        matricentemp=genfromtxt("elementcopy.txt", delimiter=',')
        matricentemp[:,0]=matricentemp[:,0]+10000000 				#max number of nodes
        matricentemp=matricentemp.astype(int)

        np.set_printoptions(threshold=1000000000000000000)
        temp=np.array2string(matricentemp,separator=',')
        temp = temp.translate(string.maketrans('', ''), '[')
        temp = temp.translate(string.maketrans('', ''), ']')
        temp="*Element, type=M3D8\n"+temp+"\n*Nset, nset=Laminate1 \n"
        for line in fileinput.FileInput("Job-1.inp",inplace=1):
            if "*Nset, nset=Laminate1" in line:
                line=line.replace(line,temp)
            print line,

        temp=np.array2string(matricentemp[:,0],separator=',')
        temp = temp.translate(string.maketrans('', ''), '[')
        temp = temp.translate(string.maketrans('', ''), ']')
            
        stringstart1="*Elset, elset=UMatOut\n"
        stringstart=stringstart1+temp +"\n"
            
        #creating string for UEL replacement line
        stringuel1="*UEL PROPERTY, ELSET=Laminate1 \n"
        stringuel2=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
        str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
        str(G12) + ", " + "0.0" + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
        ", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
        stringuel3=" \n*UEL PROPERTY, ELSET=Laminate2 \n"
        stringuel4=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
        str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
        str(G12) + ", " + str(thetamat) + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
        ", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
        stringuel5=" \n*UEL PROPERTY, ELSET=Laminate3 \n"
        stringuel6=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
        str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
        str(G12) + ", " +"0.0" + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
        ", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)
        stringuel7=" \n*UEL PROPERTY, ELSET=Laminate4 \n"
        stringuel8=str(E1) + ", " +str(nu23) + ", " + str(G23) +", " + str(E2) + ", " + \
        str(nu13) + ", " + str(G13) +", " + str(E3) + ", " +str(nu12) + ", \n" + \
        str(G12) + ", " + str(-thetamat) + ", " + str(thetaB) + ", " + str(e110) + ", " + str(e330) + \
        ", " + str(gamma120) + ", " + str(gamma130) + ", " + str(gamma230)

        stringuelumat=" \n*Membrane Section, elset=UMatOut, material=UMatOutMat \n1.,"
        stringuel10=" \n*End Part \n"
        stringuel=stringstart+stringuel1+stringuel2+stringuel3+stringuel4+stringuel5+stringuel6+\
        stringuel7+stringuel8+stringuelumat+stringuel10
            
        for line in fileinput.FileInput("Job-1.inp",inplace=1):
            if "*End Part" in line:
                line=line.replace(line,stringuel)
            print line,
            
        stringumat2="""*End Assembly
        **  THE LINES BELOW WERE EDITED TO DEFINE THE USER MATERIAL 
        *Material, name=UMatOutMat
        *user material, constants=1, type=mechanical
        0
        ** This defines the number of state variables
        *DEPVAR
        12
        """
        for line in fileinput.FileInput("Job-1.inp",inplace=1):
            if "*End Assembly" in line:
                line=line.replace(line,stringumat2)
            print line,

        counter = 0
        for line in fileinput.input('Job-1.inp', inplace=True):
            if not counter:
                if line.startswith('** OUTPUT REQUESTS'):
                    counter = 100
                else:
                    print line,
            else:
                counter -= 1
                

        test=open("Job-1.inp","a+")
        test.write("""** OUTPUT REQUESTS
        ** 
        *Restart, write, frequency=0
        ** 
        ** FIELD OUTPUT: F-Output-1
        ** 
        *Output, field
        *Node Output
        U, 
        *Element Output, directions=YES
        E, S, SDV
        ** 
        ** The lines below request output to be printed to the .dat file
        **
        ** SDV are the element state variables
        **
        *el print, freq=1
        SDV
        *node print, freq=1 
        COORD,U,RF
        **
        **  The lines below request data to be printed to .fil output
        **
        **  These data can be read for post-processing
        **
        *FILE FORMAT, ASCII
        *EL FILE
        ,
        SDV
        *NODE FILE
        COORD,U,RF
        *End Step""")
        test.close()


        ####################################submit the job
        realjob = mdb.JobFromInputFile(name='Auelelement',
            inputFileName='Job-1.inp', 
            type=ANALYSIS, atTime=None, waitMinutes=0, waitHours=0, queue=None, 
            memory=90, memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
            explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, 
            userSubroutine='OA2D.f',  
            scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=1, 
            numGPUs=0)
        mdb.jobs['Auelelement'].submit(consistencyChecking=OFF)

        #waiting
        realjob.waitForCompletion()
    
        if iiii==0:
            session.viewports['Viewport: 1'].setValues(displayedObject=session.openOdb(name='Auelelement.odb'))
                        
            copyfile('Auelelement.odb', 'Auelelement0.odb')

            #: Define Path
            session.Path(name='Path-1', type=POINT_LIST, expression=((l, h, 0.0), (l, 0.0, 0.0)))
            #: Extract U2 along path
            session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV12', outputPosition=INTEGRATION_POINT)
            session.XYDataFromPath(name='s33', path=session.paths['Path-1'], includeIntersections=True, 
                pathStyle=PATH_POINTS, numIntervals=10, shape=UNDEFORMED, 
                labelType=TRUE_DISTANCE)
            x0 = session.xyDataObjects['s33']
            s33_=(x0[(-2)])[1] #0
            
            session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV7', outputPosition=INTEGRATION_POINT)
            session.XYDataFromPath(name='s11', path=session.paths['Path-1'], includeIntersections=True, 
                pathStyle=PATH_POINTS, numIntervals=10, shape=UNDEFORMED, 
                labelType=TRUE_DISTANCE)
            x1 = session.xyDataObjects['s11']
            s11_=(x1[(-2)])[1] #0
                
            session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV10', outputPosition=INTEGRATION_POINT)
            session.XYDataFromPath(name='s13', path=session.paths['Path-1'], includeIntersections=True, 
                pathStyle=PATH_POINTS, numIntervals=10, shape=UNDEFORMED, 
                labelType=TRUE_DISTANCE)
            x2 = session.xyDataObjects['s13']
            s13_=(x2[(-2)])[1] #0
            
            session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(variableLabel='SDV1', outputPosition=INTEGRATION_POINT)
            session.XYDataFromPath(name='e11', path=session.paths['Path-1'], includeIntersections=True, 
                pathStyle=PATH_POINTS, numIntervals=10, shape=UNDEFORMED, 
                labelType=TRUE_DISTANCE)
            x3 = session.xyDataObjects['e11']
            e11_=(x3[(0)])[1]

            sigma11array=np.zeros((len(x1)-1)*2+2)
            sigma13array=np.zeros((len(x1)-1)*2+2)
            
            for j in range(len(x1)):
                s33_=(x0[j])[1]
                s11_=(x1[j])[1]
                s13_=(x2[j])[1]
                s_=np.array([s11_,s33_,s13_])
                sl_=np.dot((T3),s_) 
                
                sigma11array[2*j]=abs((sl_[0,1]))
                sigma13array[2*j]=-(sl_[0,2])
                sigma11array[2*j+1]=(abs((sl_[0,1]))+abs((sl_[0,1])))*0.5
                sigma13array[2*j+1]=-((sl_[0,2])+(sl_[0,2]))*0.5
            sigma11array[-1]=0.
            sigma11array[-2]=0.
            sigma13array[-1]=0.
            sigma13array[-2]=0.
            
            sigma11array=sigma11array[::-1]
            sigma13array=sigma13array[::-1]
            sigma11array=np.delete(sigma11array,0)
            sigma13array=np.delete(sigma13array,0)
            
        if iiii==1:
            copyfile('Auelelement.odb', 'Auelelement1.odb')

            ####################################extract displacements U
            #find result table in .dat file
            test2=open("Auelelement.dat","r")
            linetop=0
            linebottom=0
            for i,line in enumerate(test2,1):
                if "THE FOLLOWING TABLE IS PRINTED FOR ALL NODES" in line:
                    linetop=i
                elif "THE ANALYSIS HAS BEEN COMPLETED" in line:	
                    linebottom=i
                    break
            test2.close()

            lines=open("Auelelement.dat").readlines()
            open("data_behandling.txt",'w').writelines(lines[linetop+4:linebottom-9])	

            matricen=np.loadtxt("data_behandling.txt")
            NodeNr=matricen[:,0]
            COOR1=matricen[:,1]
            COOR2=matricen[:,2]
            U1=matricen[:,4]
            U2=matricen[:,5]
            U3=matricen[:,6]
            RF1=matricen[:,7]
            RF2=matricen[:,8]
            RF3=matricen[:,9]

            #modification of the displacements
            index=COOR1>l-nle/3
            COOR2=COOR2[index]
            U1=U1[index]
            U2=U2[index]
            U3=U3[index]

            index=COOR2>-nle/6 
            COOR2=COOR2[index]
            U1=U1[index]
            U2=U2[index]
            U3=U3[index]

            #sort
            arg=np.argsort(COOR2)
            COOR2=COOR2[arg]
            U1=U1[arg]
            U2=U2[arg]
            U3=U3[arg]

            #find normal and translational displacement
            UN=(U1*sin(thetaB)-U3*cos(thetaB))*2;	
            UT=(U1*cos(thetaB)+U3*sin(thetaB))*2;

            UN[UN>0]=0
    

    ####################################Calculation of the energy release rate

    #Integration with Simpson's Rule
    sim_areas=np.zeros((len(COOR2)-1.0)/2.0)
    for i in range(0,len(COOR2)-1,2):
        sim_areas[i/2]=(COOR2[i+2]-COOR2[i])

    Gss11simp=0.0;
    Gss13simp=0.0;
    for i in range(int((len(COOR2)-1.0)/2.0)):
        Gss11simp=(Gss11simp+1.0/6.0*sim_areas[i]*( (abs(UN[2*i]*sigma11array[2*i])+4.0*abs(UN[2*i+1]*sigma11array[2*i+1])+\
    abs(UN[2*i+2]*sigma11array[2*i+2])))/(h*2.0))
        Gss13simp=(Gss13simp+1.0/6.0*sim_areas[i]*((abs(UT[2*i]*sigma13array[2*i])+4.0*abs(UT[2*i+1]*sigma13array[2*i+1])+\
    abs(UT[2*i+2]*sigma13array[2*i+2]))  )/(h*2.0))
    Gsssimp=(Gss11simp+Gss13simp)

    print("Gss_simpsons",Gsssimp)
    print("normalized11_simpsons",Gss11simp/e110/h/sigmax/2)
    print("normalized13_simspons",Gss13simp/e110/h/sigmax/2)
    print("normalized_simpsons",Gsssimp/e110/h/sigmax/2)
        
    print("completed")
        
    Gss11=Gss11simp/e110/h/sigmax/2
    Gss13=Gss13simp/e110/h/sigmax/2
    listGss11[0,iii]=Gss11
    listGss13[0,iii]=Gss13
    listlh[0,iii]=listt[iii]
    
np.savetxt(NameOutput+'GI.txt',listGss11,delimiter=', ')
np.savetxt(NameOutput+'GII.txt',listGss13,delimiter=', ')
np.savetxt(NameOutput+'lh.txt',listlh,delimiter=', ')

print("completed")
