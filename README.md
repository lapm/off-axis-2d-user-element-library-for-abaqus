# Off-axis 2D user element library for Abaqus
Here you can find a collection of abaqus user element made for a off-axis 2D models which are used currently are used for modelling off-axis cracks in a laminate.

The current elements is:
- OA2DT3: Linear triangle element (3 nodes)
- OA2DT6: Quadratic triangle element (6 nodes)
- OA2DQ4: Linear quad element (4 nodes)
- OA2DQ8: Quadratic quad element (8 nodes)


# References
If used please make refererence to at least one of the following publications. The code to the specific references can be found in the folder called Ref1Scripts, Ref2Scripts, ...

[1] Mikkelsen, L.P., Legarth, B.N., Herrmann, L., Christensen, M.M., Niordson, C.F., A special finite element method applied to off-axis tunnel cracking in laminates, Engineering Fracture Mechanics, submitted July 2021.

[2] Herrmann, L., Mikkelsen, L.P., Legarth, B.N., Duddeck, F., Niordson, C.F., An efficient stiffness degradation model for layered composites with arbitrarily oriented tunneling and delamination cracks. Composites Science and Technology, submitted Jan. 2022.

[3] Herrmann, L., Mikkelsen, L.P., Legarth, B.N., Niordson, C.F., The influence of the fibre-microstructure on off-axis tunnel cracking in laminates, Under preparation.

